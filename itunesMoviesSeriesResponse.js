const { BaseHtmlParser, ValueGrabber } = require('crawl-e/v0.5.2');

class ItunesMoviesSeriesResponse extends BaseHtmlParser {

    constructor() {
        super()

        // setup ValueGrabbers  // <<< [1]
        this.genreTitleGrabber = new ValueGrabber('li') // <<< Grab title

    }

    parsePostsList(response, context, callback) {
        let { container, parsingContext } = this.prepareHtmlParsing(response.text, context)

        this.parseList(
            container,
            parsingContext,
            'posts',
            { box: 'div.grid3-column div.column ul' },
            (box, context, cb) => {
                /*no implementation yet*/
                cb(null, this.parsePostBox(box, context))
            },
            callback
        )
    }
    parsePostBox(box, context) {
        return {
            title: this.genreTitleGrabber.grabAll(box, context),
        }
    }
}

module.exports = ItunesMoviesSeriesResponse;