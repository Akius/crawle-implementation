//Imports

const { DefaultContext, DefaultRequestMaker, DefaultLogger, JsonFileWriter } = require('crawl-e/v0.5.2');
const ItunesResponseParser = require('./ItunesResponseParser');
const ItunesMoviesSeriesResponse = require('./itunesMoviesSeriesResponse');

//url
let url = 'https://itunes.apple.com/at/genre/filme/id33';


let context = new DefaultContext();
let logger = new DefaultLogger();
let requestMaker = new DefaultRequestMaker()
let responseParser = new ItunesResponseParser();
let itunesMoviesSeriesResponseParser = new ItunesMoviesSeriesResponse()
let outputWriter = new JsonFileWriter()

//The returned output
let Data = []

const getUrlAndGenre = async () => {
    requestMaker.logger = logger;
    responseParser.logger = logger;
    outputWriter.logger = logger
    let urlArrays = [];
    let genreArrays = [];
    return await requestMaker.get(url, context, async (err, res) => {
        if (err) {
            console.error(`Oh no!!!, something went wrong: ${err}`)
        }

        await responseParser.parsePostsList(res, context, async (err, posts) => {
            await posts.forEach((val) => {
                urlArrays.push(...val.url)
                genreArrays.push(...val.title)
            })

            getMoviesAndSeries(urlArrays, genreArrays)
        })

    })
}


const getMoviesAndSeries = async (urls, genres) => {

    itunesMoviesSeriesResponseParser.logger = logger;

    for (let index = 0; index < urls.length; index++) {

        //List of movies
        let movies = []
        requestMaker.get(urls[index], context, async (err, res) => {
            if (err) {
                console.error(`Oh no!!!, something went wrong: ${err}`)
            }

            await itunesMoviesSeriesResponseParser.parsePostsList(res, context, async (err, data) => {

                data.forEach((val) => {
                    movies.push(...val.title)
                })

            })
            Data.push({
                genre: genres[index],
                url: urls[index],
                title: movies
            })
            return output(Data, context);

        })
    }


}


const output = async (Data, context) => {

    await outputWriter.saveFile(Data, context, (err) => {
        if (err) {
            console.error(`Oh noo, sth. wen't wrong: ${err}`)
            return
        }
        console.log('All Done', '👏')
    })

}

getUrlAndGenre();


