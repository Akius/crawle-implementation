## VOD Movies Scrapper

This project scrapes "all" genre  from this 'https://itunes.apple.com/at/genre/filme/id33'. And all movies associated with each genre.


### Expected outcome
- Return all genre,
- Return all genre url,
- Return all movie title associated with each genre,

### Result

- All genre are returned,
- All genre url are returned,
- All movie title are returned.


### How to use app

1. Clone repository,
2. install dependency using, yarn install,
3. Run app with node and filename. E.g "node itunes-crawler.js",


#### Thank You