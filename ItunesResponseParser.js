const { BaseHtmlParser, ValueGrabber } = require('crawl-e/v0.5.2');

class ItunesResponseParser extends BaseHtmlParser {

    constructor() {
        super()

        // setup ValueGrabbers  
        this.genreTitleGrabber = new ValueGrabber('li') // <<< Grab title
        this.genreUrlGrabber = new ValueGrabber('li a @href')  // <<< Grab url


    }

    parsePostsList(response, context, callback) {
        let { container, parsingContext } = this.prepareHtmlParsing(response.text, context)

        this.parseList(
            container,
            parsingContext,
            'posts',
            { box: '.grid3-column ul.list.column' },
            (box, context, cb) => {
                cb(null, this.parsePostBox(box, context))
            },
            callback
        )
    }
    parsePostBox(box, context) {
        return {
            title: this.genreTitleGrabber.grabAll(box, context),
            url: this.genreUrlGrabber.grabAll(box, context)
        }
    }
}

module.exports = ItunesResponseParser;